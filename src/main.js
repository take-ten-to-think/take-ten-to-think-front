import Vue from "vue";
import App from "./App.vue";
import {
  BootstrapVue,
  IconsPlugin
} from "bootstrap-vue";
import router from "./router";

import {
  httpClient
} from "./services/http-client";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import VueSimpleMarkdown from "vue-simple-markdown";
import "vue-simple-markdown/dist/vue-simple-markdown.css";
import Vuex from "vuex";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueSimpleMarkdown);
Vue.use(Vuex);

Vue.prototype.$http = httpClient;

const store = new Vuex.Store({
  state: {
    user: null,
    createTopicFormContent: null,
    updateTopicFormContent: null,
    messages: []
  },
  getters: {
    userId: state => {
      return (state.user) ? state.user.userId : null;
    }
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    unsetUser(state) {
      state.user = null;
    },
    storeCreateTopicFormContent(state, topic) {
      state.createTopicFormContent = topic;
    },
    removeCreateTopicFormContent(state) {
      state.createTopicFormContent = null;
    },
    storeUpdateTopicFormContent(state, topic) {
      state.updateTopicFormContent = topic;
    },
    removeUpdateTopicFormContent(state) {
      state.updateTopicFormContent = null;
    },
    addErrorMessage(state, message) {
      state.messages.push({
        message: message,
        level: "error"
      });
    },
    addInfoMessage(state, message) {
      state.messages.push({
        message: message,
        level: "info"
      });
    },
    addSuccessMessage(state, message) {
      state.messages.push({
        message: message,
        level: "success"
      });
    },
    clearMessages(state) {
      state.messages = []
    }
  },
  actions: {
    loadProfile({
      commit
    }) {
      httpClient.get('/profile')
        .then(response => {
          commit('setUser', response.data);
        }).catch(() => {
          commit('unsetUser');
        });
    }
  }
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");