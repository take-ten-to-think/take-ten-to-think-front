const jwtTokenStorageName = 'jwtToken'

const getToken = () => {
    return localStorage.getItem(jwtTokenStorageName);
};

const setToken = (token) => {
    localStorage.setItem(jwtTokenStorageName, token);
};

const hasToken = () => {
    return !!getToken();
};

const removeToken = () => {
    localStorage.removeItem(jwtTokenStorageName);
};

function authTokenService() {
    return {
        getToken,
        setToken,
        hasToken,
        removeToken,
    };
}

export default authTokenService()