import axios from "axios";
import authTokenService from './auth-token-service'
import router from '../router';

let config = {
  baseURL: "http://localhost:3000/",
};

const httpClient = axios.create(config);

httpClient.interceptors.request.use((config) => {
  if (authTokenService.hasToken()) {
    config.headers.common["Authorization"] = `Bearer ${authTokenService.getToken()}`;
  }
  return config;
});

httpClient.interceptors.response.use(response => (response), error => {
  if (error.response && error.response.status == 401) {
    router.push({
      name: "Login",
    });
  }
  if (error.response && [404, 403].includes(error.response.status)) {
    router.push({
      name: "Home",
    });
  }
  return Promise.reject(error);
});

export {
  httpClient
};