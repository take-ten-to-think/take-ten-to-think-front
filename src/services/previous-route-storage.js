const RouteFormStorageContentKey = "RouteFormStorageContentKey";


const storeRoute = (route) => {
    localStorage.setItem(RouteFormStorageContentKey, JSON.stringify(route))
};

const pop = () => {
    const ret = JSON.parse(localStorage.getItem(RouteFormStorageContentKey));
    localStorage.removeItem(RouteFormStorageContentKey);
    return ret;
};

const previousRouteStorage = () => ({
    storeRoute,
    pop
});

export default previousRouteStorage()