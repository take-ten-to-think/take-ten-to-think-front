import Vue from "vue";
import VueRouter from "vue-router";
import TopicsView from "../views/TopicsView.vue";
import previousRouteStorage from "../services/previous-route-storage";

Vue.use(VueRouter);

const routes = [{
    path: "/login",
    name: "Login",
    component: () => import("../views/LoginView.vue"),
  }, {
    path: "/schedules",
    name: "SchedulesView",
    component: () => import("../views/SchedulesView.vue"),
  }, {
    path: "/schedules/create",
    name: "ScheduleCreation",
    component: () => import("../views/ScheduleCreationView.vue"),
  },
  {
    path: "/topics",
    name: "TopicsView",
    component: () => import("../views/TopicsView.vue"),
  }, {
    path: "/topics/create",
    name: "TopicCreation",
    component: () => import("../views/TopicCreationView.vue"),
  }, {
    path: "/topics/:id/edit",
    name: "EditTopic",
    component: () => import("../views/EditTopicView.vue"),
  }, {
    path: "/topics/:id",
    name: "TopicDetail",
    component: () => import("../views/TopicDetailsView.vue"),
  }, {
    path: "/topics/:id/create-presentation",
    name: "CreateTopicPresentationView",
    component: () => import("../views/CreateTopicPresentationView.vue"),
  }, {
    path: "/*",
    name: "Home",
    component: TopicsView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((_, from, next) => {
  previousRouteStorage.storeRoute(from.fullPath);
  next();
});

export default router;